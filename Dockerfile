FROM registry.gitlab.com/fdroid/ci-images-client:latest
MAINTAINER hans@eds.org

run apt-get update \
	&& apt-get -qy dist-upgrade \
	&& apt-get -qy install --no-install-recommends \
            build-essential \
            gnupg \
            libx11-dev \
            pkg-config \
	&& apt-get -qy autoremove --purge \
	&& apt-get clean \
	&& rm -rf /var/lib/apt/lists/*

# SDK components - the android tool is too dumb to with its license
# prompting so we have to install the packages one at a time to get
# reliability.
#
# Also, the emulator can't find its own libraries from the SDK with
# LD_LIBRARY_PATH.
ENV ANDROID_NDK_HOME="$ANDROID_HOME/ndk-bundle" \
    GOPATH="/go"

ENV PATH="/usr/local/go/bin:$GOPATH/bin:$PATH"

COPY test /

RUN	   echo y | sdkmanager "ndk-bundle" > /dev/null \
	&& echo y | $ANDROID_HOME/tools/bin/sdkmanager --update > /dev/null

RUN cd /usr/local \
	&& export gotarball="go1.11.1.linux-amd64.tar.gz" \
	&& wget -q http://storage.googleapis.com/golang/${gotarball} \
	&& wget -q http://storage.googleapis.com/golang/${gotarball}.asc \
	&& curl https://dl.google.com/linux/linux_signing_key.pub | gpg --no-tty --batch --import \
	&& gpg --verify ${gotarball}.asc \
	&& echo "2871270d8ff0c8c69f161aaae42f9f28739855ff5c5204752a8d92a1c9f63993  ${gotarball}" | sha256sum -c \
	&& tar -xzf ${gotarball} \
	&& go get github.com/smartystreets/goconvey/convey \
	&& go get -u golang.org/x/mobile/cmd/gomobile \
	&& cd $GOPATH/src/golang.org/x/mobile/cmd/gomobile \
	&& go install \
	&& echo $PATH \
	&& echo $GOPATH \
	&& ls -l $GOPATH/bin || ls -l $GOPATH \
	&& gomobile init -ndk $ANDROID_NDK_HOME
